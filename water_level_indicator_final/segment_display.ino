void segmentDisplay(int x) {
  if (x == 0) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], HIGH);
    }
    digitalWrite(segment[6], LOW);
  }
  else if (x == 1) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], LOW);
    }
    digitalWrite(segment[1], HIGH);
    digitalWrite(segment[2], HIGH);
  }
  else if (x == 2) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], HIGH);
    }
    digitalWrite(segment[2], LOW);
    digitalWrite(segment[5], LOW);
  }
  else if (x == 3) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], HIGH);
    }
    digitalWrite(segment[4], LOW);
    digitalWrite(segment[5], LOW);
  }
  else if (x == 4) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], HIGH);
    }
    digitalWrite(segment[0], LOW);
    digitalWrite(segment[3], LOW);
    digitalWrite(segment[4], LOW);
  }
  else if (x == 5) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], HIGH);
    }
    digitalWrite(segment[1], LOW);
    digitalWrite(segment[4], LOW);
  }
  else if (x == 6) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], HIGH);
    }
    digitalWrite(segment[1], LOW);
  }
  else if (x == 7) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], LOW);
    }
    digitalWrite(segment[0], HIGH);
    digitalWrite(segment[1], HIGH);
    digitalWrite(segment[2], HIGH);
  }
  else if (x == 8) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], HIGH);
    }
  }
  else if (x == 9) {
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], HIGH);
    }
    digitalWrite(segment[4], LOW);
  }
  else { // any other constrained value
    for (int i = 0; i < 7; i++) {
      digitalWrite(segment[i], LOW);
    }
    digitalWrite(segment[6],HIGH);
  }
}
