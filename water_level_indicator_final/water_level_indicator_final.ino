const int trigPin = 3;
const int echoPin = 4;
const int vcc = 2; // sensor power
const int bell = 5; // bell relay
const int buzzer = 6;
const int fillingLed = 14;
const int lowLed = 16;

int levelPre, levelNew, cm = 0; // heights of waterlevels

int sensorToBottom = 200; // distance between sensor and bottom
int maxWaterLevel = 200; // maximum level of the water
int minWaterLevel = 20; // minimum level of water and low water indicating level
int mapValue; //maped value between 0,9 of waterlevel
int segment[7] = {7, 8, 9, 10, 11, 12, 13};

long duration, duration_1, duration_2;// measurements of distance

boolean filling, empty = false;

int fillStart;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#include <RealTimeClockDS1307.h>

char formatted[] = "00-00-00 00:00:00x";
int Minutes, Seconds, Hours = 0;

void setup() {
  Serial.begin(9600);

  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(vcc, OUTPUT);
  pinMode(bell, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(fillingLed, OUTPUT);
  pinMode(lowLed, OUTPUT);

  for (int i = 0; i < 7; i++) {
    pinMode(segment[i], OUTPUT);
  }
  delay(2000);

  measure();
  mapValue = map(levelNew, minWaterLevel, maxWaterLevel, 0, 9);
  segmentDisplay(mapValue);
  levelPre = levelNew;
}

void loop() {
  processCommand();
  RTC.readClock();
  Hours = RTC.getHours();
  Minutes = RTC.getMinutes();
  Seconds = RTC.getSeconds();
  RTC.getFormatted(formatted);

  if (Seconds < 3) {
    measure();
    mapValue = map(levelNew, 0, maxWaterLevel, 0, 9);
    segmentDisplay(mapValue);
  }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  if (levelNew < minWaterLevel) {
    digitalWrite(lowLed, HIGH);
    empty = true;
  }
  else{
    digitalWrite(lowLed,LOW);
    empty=false;
  }
  if (Seconds<2 && empty){
    digitalWrite(buzzer,HIGH);  
  }
  else{
    digitalWrite(buzzer,LOW);  
  }
//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  if (levelNew - levelPre < 10) {
    filling = true;
    digitalWrite(fillingLed, HIGH);
  }
  if(filling && (Seconds<3 || abs(Seconds-30<3)) 
  

}




void printing() {
  Serial.println(" ");
  Serial.print("levelNew=");
  Serial.print(levelNew);
  Serial.println(" ");
  Serial.print("map value=");
  Serial.print(mapValue);
  Serial.println(formatted);
}




void filled() {
  digitalWrite(buzzer, HIGH);
  digitalWrite(bell, HIGH);
  delay(200);
  digitalWrite(bell, LOW);
  delay(500);
  digitalWrite(bell, HIGH);
  delay(300);
  digitalWrite(bell, LOW);
  delay(1000);
  digitalWrite(buzzer, LOW);
  digitalWrite(fillingLed,LOW)
}



//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


