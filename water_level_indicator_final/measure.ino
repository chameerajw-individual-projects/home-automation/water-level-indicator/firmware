int measure() {
  levelPre = levelNew;
  digitalWrite(vcc, HIGH);
  delay(100);
  digitalWrite(trigPin, LOW);
  delayMicroseconds(10);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration_1 = pulseIn(echoPin, HIGH); //taking the first reading
  delay(50);
  digitalWrite(trigPin, LOW);
  delayMicroseconds(10);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration_2 = pulseIn(echoPin, HIGH); //taking the second reading
  digitalWrite(vcc, LOW);
  if ((abs(duration_1 - duration_2) < 1000) && duration_1 < 13000 ) { // if both readings are nearly equal duration is the mean
    duration = (duration_1 + duration_2) / 2;
  }
  else {
    measure();
  }
  cm = duration / 29 / 2;
  levelNew = (sensorToBottom - cm); //assigning height of water level

  // Serial.print("duration=");
  //Serial.print(duration);
  //Serial.print(" ");
  //Serial.print(inches);
  //Serial.print("in, ");
  //Serial.printlncm);
  //Serial.print("cm, ");
  //Serial.print("cm=");
  //Serial.println(duration);

  return levelNew;
}
